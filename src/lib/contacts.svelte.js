// =====================
// D A T A + Read
// =====================
export let contacts = $state([]);

// CRUD - M E T H O D S
// Create
export const createContact = (contact) => {
    contact.id = crypto.randomUUID();
    contacts.push(contact);

    writeToLocalStorage();
};

// Update
export const updateContact = (contact) => {
    const index = contacts.findIndex((c) => c.id === contact.id);
    contacts[index].name = contact.name;
    contacts[index].phone = contact.phone;

    writeToLocalStorage();
};

// Delete
export const deleteContact = (id) => {
    // get index of element with this id
    const index = contacts.findIndex(
        (contact) => contact.id === id
    );

    // delete element with index from array
    contacts.splice(index, 1);

    // Save new array in localStorage
    writeToLocalStorage();
};

// =====================
// GUI-Variables
// =====================
export let formVisible = $state({ state: "main" });
/* 
        'main': table "Kontakte"
        'delete': form "Kontakt löschen"
        'edit': form "Kontakt bearbeiten"
*/
export let contactToEdit = $state({});
export let contactToDelete = $state({
    id: "",
    name: ""
});

// =====================
// L O C A L S T O R A G E 
// =====================
const writeToLocalStorage = () => {
    const jsonString = JSON.stringify(contacts);
    localStorage.setItem("t3a-kontakte", jsonString);
};

export const readFromLocalStorage = () => {
    const jsonString = localStorage.getItem("t3a-kontakte");

    let arrayFromStorage = [];

    if (jsonString) {
        // there is data in localstorage
        arrayFromStorage = JSON.parse(jsonString);

        // delete all old elements
        contacts.length = 0;

        // add all elements of array to contacts
        /*for (let i = 0; i < array.length; i++) {
            contacts.push(array[i]);
        }*/

        arrayFromStorage.forEach(
            contact => {
                contacts.push(contact);
            }
        );
    } else {
        // jsonString is null
    }
}
