REM build-Verzeichnis komplett löschen
rd /s /q "build"

REM build
call npm run build

REM komplett neues Repository in build
REM mit Branch 'pages' erstellen
cd build
git init
git checkout -B pages
git add -A
git commit -m 'deploy'

REM aktuelle Version hochladen
REM alte Version wird gelöscht
git push --force -u https://codeberg.org/meisterk/t3a-kontakte.git pages

REM wieder zurückspringen
cd ..